module.exports = {
    root: true,
    env: {
        browser: true,
        node: true,
    },
    extends: [
        "eslint:recommended",
        "plugin:vue/recommended",
        "plugin:prettier/recommended",
        "prettier/vue"
    ],
    plugins: ["prettier", "vue"],
    // add your custom rules here
    rules: {
        'multiline-ternary': ["error", "always-multiline"],
    },
}
