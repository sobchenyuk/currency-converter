export default {
    methods: {
        $_canceling() {
            this.$store.dispatch("GET_AMOUNT", 0);
            this.$router.push("/");
        },
    },
};
