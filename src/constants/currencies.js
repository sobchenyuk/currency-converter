/*
 * USD: "$",
 * UAH: "₴",
 */

export default {
    USD: "$",
    UAH: "₴",
};
