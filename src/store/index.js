import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        amount: 0,
        selectedCurrency: localStorage.getItem("selectedCurrency") || 1,
        currencies: {},
        isCurrencies: false,
        amountResult: JSON.parse(localStorage.getItem("amountResult")) || [],
    },
    mutations: {
        SET_CURRENCY(state, currencies) {
            state.currencies = currencies;
        },
        SET_IS_CURRENCY(state, isCurrencies) {
            state.isCurrencies = isCurrencies;
        },
        SET_AMOUNT(state, amount) {
            state.amount = amount;
        },
        SET_SELECTED_CURRENCY(state, currency) {
            localStorage.setItem("selectedCurrency", currency);
            state.selectedCurrency = currency;
        },
        SET_AMOUNT_RESULT(state, amountResult) {
            localStorage.setItem("amountResult", JSON.stringify(amountResult));
            state.amountResult = amountResult;
        },
    },
    actions: {
        /*
         * по условию задачи я делаю обменку (USD to UAH).
         * поэтому получаю только одну позицию
         */

        async GET_CURRENCY(state) {
            state.commit("SET_IS_CURRENCY", false);
            try {
                const { data } = await axios.get(
                    "https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11"
                );
                state.commit("SET_IS_CURRENCY", true);
                state.commit("SET_CURRENCY", data[0]);
            } catch (err) {
                state.commit("SET_IS_CURRENCY", false);
                console.log(err);
            }
        },
        GET_AMOUNT(state, amount) {
            state.commit("SET_AMOUNT", amount);
        },
        GET_SELECTED_CURRENCY(state, currency) {
            state.commit("SET_SELECTED_CURRENCY", currency);
        },
        GET_AMOUNT_RESULT(state, amountResult) {
            state.commit("SET_AMOUNT_RESULT", amountResult);
        },
    },
    getters: {
        filterCurrencies: (state) => {
            const { currencies } = state;
            return [
                {
                    exchange: currencies.ccy + " to " + currencies.base_ccy,
                    fromCurrency: currencies.ccy,
                    toCurrency: currencies.base_ccy,
                    course: currencies.buy,
                },
                {
                    exchange: currencies.base_ccy + " to " + currencies.ccy,
                    fromCurrency: currencies.base_ccy,
                    toCurrency: currencies.ccy,
                    course: currencies.sale,
                },
            ];
        },
    },
});
