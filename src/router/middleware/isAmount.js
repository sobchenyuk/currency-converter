export default function isAmount({ next, store }) {
    if (Number(store.state.amount) <= 0) {
        return next({
            name: "Home",
        });
    }

    return next();
}
