import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";

import store from "@/store";
import isAmount from "./middleware/isAmount";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/pair-selection",
        name: "PairSelection",
        component: () =>
            import(
                /* webpackChunkName: "pair-selection" */ "../views/pair-selection.vue"
            ),
        meta: {
            middleware: [isAmount],
        },
    },
    {
        path: "/result",
        name: "Result",
        component: () =>
            import(/* webpackChunkName: "result" */ "../views/result.vue"),
        meta: {
            middleware: [isAmount],
        },
    },
];

const router = new VueRouter({
    routes,
    mode: "history",
});

router.beforeEach((to, from, next) => {
    if (!to.meta.middleware) {
        return next();
    }
    const middleware = to.meta.middleware;
    const context = {
        to,
        from,
        next,
        store,
    };
    return middleware[0]({
        ...context,
    });
});

export default router;
